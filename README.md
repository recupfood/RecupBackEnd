# RecupBackEnd

## installation

- installer docker et docker compose
- `docker-compose up -d --build` à la racine du projet lancer la commande 
- `docker exec php composer install` pour installer le gestionnaire de dépandance composer 

## Commandes utiles

### docker

- `docker exec -ti [name] bash` pour aller dans le conteneur indiquer
- `docker-compose down` arrête et supprime les conteneurs
- `docker-compose stop` arrête les conteneurs
- `docker ps` permet de voir les conteneurs lancés
- `docker images` permet de voir toutes les images téléchargers
- `docker image prune` permet clean des images non utilisées

## Dépendances symfony

- `composer req annotations` permet les annotations route pour controller
- `composer req asset` permet path relative pour les templates
- `composer req twig` twig
- `composer req make --dev` permet de crée entité/controlleur/form à partir du CLI
- `composer req debug-bundle` télécharge le bundle dubug
- `composer req orm-bundle` télécharge le bundle doctrine
- `composer req serializer` permet traitement json
- `composer req validation` validation pour ORM
- `composer req http-foundation` requete json
- `composer req profiler` debug barre dans la vue
- `composer req form` importation des form type

