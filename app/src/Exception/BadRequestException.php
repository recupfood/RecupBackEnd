<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class BadRequestException extends ApplicationException
{
    public function __construct(string $message = "les donnnées reçu ne sont pas valides",
                                int $code = Response::HTTP_BAD_REQUEST)
    {
        parent::__construct($message, $code);
    }

}