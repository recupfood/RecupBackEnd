<?php

namespace App\Exception;


use Symfony\Component\HttpFoundation\Response;

class NotFoundException extends ApplicationException
{
    public function __construct(string $message = "aucunes données correspondantes trouvées",
                                int $code = Response::HTTP_NOT_FOUND)
    {
        parent::__construct($message, $code);
    }
}