<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Entity\Recup;
use App\Form\ProduitType;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

class ProduitController extends AbstractFOSRestController
{
    private EntityManagerInterface $em;
    private ProduitRepository $produitRepository;

    /**
     * @param $em
     * @param $produitRepository
     */
    public function __construct(EntityManagerInterface  $em,
                                ProduitRepository       $produitRepository)
    {
        $this->em = $em;
        $this->produitRepository = $produitRepository;
    }


    /**
     * @Rest\Get("/produit", name = "produit_get_all")
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function getAll()
    {
        $produits = [];
        foreach ($this->produitRepository->findAll() as $produit) {
            $produits[] = $produit->toArray();
        }
        return $produits;
    }


    /**
     * @Rest\Get("/produit/recup/{id}", name="liste_produits_by_recup", requirements={"id" = "\d+"})
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function getByRecup(Recup $recup)
    {
        $lstProduits = $recup->getProduits();
        $listeProduits = [];
        foreach ($lstProduits as $produit) {
            $listeProduits[] = $produit->toArray();
        }
        return $listeProduits;
    }


    /**
     * @Rest\Get("/produit/{id}", name="produit_get_by_id", requirements={"id" = "\d+"})
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function getById(Produit $produit)
    {
        return $produit->toArray();
    }

    /**
     * @Rest\Get("/produit/search", name="produit_get_by_name")
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function getByName(Request $request)
    {
        $search = $request->get('s');
        $produit = $this->produitRepository->findOneBy(['nom' => $search]);
        return $produit !== null ? $produit->toArray() : [];
    }


    /**
     * @Rest\Post("/produit", name = "produit_create")
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     */
    public function create(Request $request)
    {
        $produit = new Produit();
        $response = $this->gestionCreateUpdate($produit, $request);

        return $response;
    }


    /**
     * @Rest\Put("/produit/{id}", name="produit_update", requirements={"id" = "\d+"})
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function update(Request $request, Produit $produit)
    {
        $response = $this->gestionCreateUpdate($produit, $request);
        return $response;
    }

    private function gestionCreateUpdate(Produit $produit, Request $request)
    {
        $form = $this->createForm(ProduitType::class, $produit);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if ($form->isValid()) {
            $this->em->persist($produit);
            $this->em->flush();

            $response = $produit->toArray();
        } else {
            $response = $form;
        }
        return $response;
    }


    /**
     * @Rest\Delete("/produit/{id}", name="produit_delete", requirements={"id" = "\d+"})
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     */
    public function delete(Produit $produit)
    {
        $this->em->remove($produit);
        $this->em->flush();
    }
}
