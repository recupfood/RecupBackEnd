<?php

namespace App\Controller;

use App\Services\ScraperService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SearchNonPersistProduitController extends AbstractController
{
    private LoggerInterface $logger;
    private ScraperService $scraperSerice;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, ScraperService $scraperSerice)
    {
        $this->logger = $logger;
        $this->scraperSerice = $scraperSerice;
    }

    /**
     * @Rest\Get("/scrap", name = "scrap_all")
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function scraper(Request $request)
    {
        $reponse = [];
        $search = $request->get('s');
        $reponse = $this->scraperSerice->searchProduit($search);
        $this->logger->warning('search input', [$reponse]);
        return new JsonResponse($reponse);
    }

    /**
     * @Rest\Get("test/scrap", name = "scrap_all_test")
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function testscrapidiScrapLocal()
    {
        $reponse = [
            0 => ["nom" => "Kev Carotte France 750 g", "prix" => 1.12, "prixUnite" => ["prixUnite" => 1.40, "unite" => "kg"]],
            1 => ["nom" => "Kev Carotte France la botte", "prix" => 1.19, "prixUnite" => ["prixUnite" => 1.19, "unite" => "pce"]],
            2 => ["nom" => "Kev Carotte des sables France 750g", "prix" => 1.39, "prixUnite" => ["prixUnite" => 1.85, "unite" => "kg"]],
            3 => ["nom" => "Kev Carotte Filière U de Normandie France, sachet 1,5kg", "prix" => 1.95, "prixUnite" => ["prixUnite" => 1.30, "unite" => "kg"]],
            4 => ["nom" => "Kev Carotte BIO France 750 g", "prix" => 1.69, "prixUnite" => ["prixUnite" => 2.25,"unite" => "kg"]],
            5 => ["nom" => "Kev Carotte selection gout France barquette 1 kg", "prix" => 1.95, "prixUnite" => ["prixUnite" => 1.95, "unite" => "kg"]],
        ];
        return new JsonResponse($reponse);
    }
}
