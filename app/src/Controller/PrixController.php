<?php

namespace App\Controller;

use App\Entity\Prix;
use App\Entity\Produit;
use App\Exception\NotFoundException;
use App\Form\PrixType;
use App\Repository\PrixRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PrixController extends AbstractFOSRestController
{
    private EntityManagerInterface $em;
    private PrixRepository $prixRepository;

    /**
     * @param $em
     * @param $prixRepository
     */
    public function __construct(EntityManagerInterface  $em,
                                PrixRepository          $prixRepository)
    {
        $this->em = $em;
        $this->prixRepository = $prixRepository;
    }


    /**
     * @Rest\Get("/prix", name = "prix_get_all")
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function getAll()
    {
        $listePrix = [];

        foreach ($this->prixRepository->findAll() as $prix) {
            $listePrix[] = $prix->toArrayComplete();
        }
        return $listePrix;
    }


    /**
     * @Rest\Get("/prix/{id}", name="prix_get_by_produit", requirements={"id" = "\d+"})
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function getlistByProduit(Produit $produit)
    {
        $lstPrix = $produit->getListePrix();
        $listePrix = [];
        foreach ($lstPrix as $prix) {
            $listePrix[] = $prix->toArray();
        }
        return $listePrix;
    }

    /**
     * @Rest\Get("/prix/last/{id}", name="dernier_prix_by_produit", requirements={"id" = "\d+"})
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function getDernierPrixProduit(Produit $produit)
    {
        $prix = $this->prixRepository->getLastByProduitAndDate($produit, new \DateTime('now'));

        if ($prix) {
            $response = $prix;
        } else {
            throw new NotFoundException("Aucun prix pour ce produit");
        }
        return $response;
    }


    /**
     * @Rest\Post("/prix", name = "prix_create")
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     */
    public function create(Request $request)
    {
        $prix = new Prix();
        $form = $this->createForm(PrixType::class, $prix);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if ($form->isValid()) {
            $this->em->persist($prix);
            $this->em->flush();

            $response = $prix->toArray();
        } else {
            $response = $form;
        }
        return $response;
    }
}
