<?php

namespace App\Controller;

use App\Entity\AssoProduitRecup;
use App\Entity\Recup;
use App\Exception\BadRequestException;
use App\Exception\NotFoundException;
use App\Form\AssoProduitRecupType;
use App\Form\RecupType;
use App\Repository\AssoProduitRecupRepository;
use App\Repository\RecupRepository;
use App\Services\CalculSommeService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RecupController extends AbstractFOSRestController
{

    private EntityManagerInterface $em;
    private RecupRepository $recupRepository;
    private AssoProduitRecupRepository $assoProduitRecupRepository;
    private CalculSommeService $calculSommeService;

    /**
     * @param EntityManagerInterface $em
     * @param RecupRepository $recupRepository
     */
    public function __construct(EntityManagerInterface     $em,
                                RecupRepository            $recupRepository,
                                AssoProduitRecupRepository $assoProduitRecupRepository,
                                CalculSommeService         $calculSommeService)
    {
        $this->em = $em;
        $this->recupRepository = $recupRepository;
        $this->assoProduitRecupRepository = $assoProduitRecupRepository;
        $this->calculSommeService = $calculSommeService;
    }


    /**
     * @Rest\Get("/recup", name = "recup_get_all")
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function getAll()
    {
        $recups = [];
        foreach ($this->recupRepository->findAll() as $recup) {

            $somme = $this->calculSommeService->additionSommeRecup($recup);
            $recup->setSomme($somme);
            $recups[] = $recup->toArray();
        }
        return $recups;
    }


    /**
     * @Rest\Get("/recup/{id}", name="recup_get_by_id", requirements={"id" = "\d+"})
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function getById(Recup $recup)
    {
        $somme = $this->calculSommeService->additionSommeRecup($recup);
        $recup = $recup->setSomme($somme)->toArray();

        return $recup;
    }


    /**
     * @Rest\Post("/recup", name = "recup_create")
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     */
    public function create(Request $request)
    {
        $recup = new Recup();
        $response = $this->gestionCreateUpdate($recup, $request);

        return $response;
    }


    /**
     * @Rest\Put("/recup/{id}", name="recup_update", requirements={"id" = "\d+"})
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function update(Request $request, Recup $recup)
    {
        $response = $this->gestionCreateUpdate($recup, $request);
        return $response;
    }

    private function gestionCreateUpdate(Recup $recup, Request $request)
    {
        $form = $this->createForm(RecupType::class, $recup);
        $data = json_decode($request->getContent(), true);

        if([$data['nom']] == [""]) {
            $date = new \DateTime();
            $data['nom'] = $date->format('d-m-Y');
        }

        $form->submit($data);

        if ($form->isValid()) {
            $this->em->persist($recup);
            $this->em->flush();

            $response = $recup->toArray();
        } else {
            $response = $form;
        }
        return $response;
    }


    /**
     * @Rest\Delete("/recup/{id}", name="recup_delete", requirements={"id" = "\d+"})
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     */
    public function delete(Recup $recup)
    {
        $this->em->remove($recup);
        $this->em->flush();
    }


    /**
     * @Rest\Post("/recup/{id}/produit", name = "recup_produit_add")
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     */
    public function addProduit(Recup $recup, Request $request)
    {
        $assoProduit = new AssoProduitRecup();
        $form = $this->createForm(AssoProduitRecupType::class, $assoProduit);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if ($form->isValid()) {
            $assoProduit->setRecup($recup);
            $assoProduit->setDernierPrix($this->calculSommeService->calculLignePrix($assoProduit));
            $recup->addAssoProduitRecup($assoProduit);
            $somme = $this->calculSommeService->additionSommeRecup($recup);
            $recup = $recup->setSomme($somme);

            $this->em->persist($recup);
            $this->em->flush();

            $response = $recup->toArray();
        } else {
            $response = $form;
        }
        return $response;
    }


    /**
     * @Rest\Put("/recup/{id}/produit", name="recup_produit_update", requirements={"id" = "\d+"})
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function updateQuantite(Recup $recup, Request $request)
    {
        $assoProduit = new AssoProduitRecup();
        $data = json_decode($request->getContent(), true);
        $form = $this->createForm(AssoProduitRecupType::class, $assoProduit);
        $form->submit($data);

        if ($form->isValid()) {
            $assoProduit = $this->assoProduitRecupRepository->findOneBy(['produit' => $data['produit'],
                                                                         'recup' => $recup]);

            $assoProduit->setQuantite($data['quantite']);
            $assoProduit->setDernierPrix($this->calculSommeService->calculLignePrix($assoProduit));
            $somme = $this->calculSommeService->additionSommeRecup($recup);
            $recup = $recup->setSomme($somme);

            $this->em->persist($assoProduit);
            $this->em->flush();

            $response = $recup->toArray();
        } else {
            $response = $form;
        }
        return $response;
    }


    /**
     * @Rest\Delete("/recup/{id}/produit", name="recup_produit_delete", requirements={"id" = "\d+"})
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     */
    public function removeProduit(Recup $recup, Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if (array_key_exists('produit', $data)) {
            $assoProduit = $this->assoProduitRecupRepository->findOneBy(['produit' => $data['produit'],
                                                                         'recup' => $recup]);

            if ($assoProduit) {
                $recup->removeAssoProduitRecup($assoProduit);
                $this->em->remove($assoProduit);
                $this->em->flush();
            } else {
                throw new NotFoundException("le produit n'est pas associcé à cette récup");
            }
        } else {
            throw new BadRequestException("le champs produit est manquant ou mal écrit");
        }
    }
}
