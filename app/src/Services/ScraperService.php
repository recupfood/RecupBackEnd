<?php

namespace App\Services;

use FOS\RestBundle\Controller\Annotations as Rest;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;

class ScraperService 
{
    const STORE_ID_COOKIE = 'storeId';
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }
    
    /**
     * @Rest\Put("/scrap", name = "scrap_all")
     * @Rest\View(statusCode=Response::HTTP_OK)
     */
    public function searchProduit(string $search)
    {
        $reponse = [];
        $valeur = str_replace(' ', '+', $search);
        $result = $this->get_web_page('https://www.coursesu.com/recherche?q='.$valeur, $this->getMagasinCookie());
        if ($result['errno'] != null) {
            throw new \Exception('aie aie aie');
        }

        $crawler = new Crawler($result['content']);
        $crawler->filter('li.grid-tile')->each(function ($node) use (&$reponse) {

            $nom = $node->filter('.name-link')->text();
            $prixResult = $node->filter('.sale-price')->extract(['data-item-price']);

            $prixUniteNode = $node->filter('.unit-info');
            $prixUnites = $prixUniteNode->count() > 0 ? $prixUniteNode->text() : 0;
            $prixUnitTab = $this->transformPrixUnite($prixUnites);

            $reponse[] = [
                'nom' => $nom,
                'prix'=> count($prixResult) > 0 ? (float) $prixResult[0] : 0,
                'prixUnite'=> $prixUnitTab
            ];
        });

        $this->logger->info('reponse', $reponse);
        return $reponse;
    }

    private function getMagasinCookie()
    {
        $reponse = $this->get_web_page('https://www.coursesu.com/drive-superu-lerheu', [], true);
//        $reponse = $this->get_web_page('https://www.coursesu.com/drive-superu-rennessaintgregoire', [], true);
        return [self::STORE_ID_COOKIE => $reponse['cookies'][self::STORE_ID_COOKIE]];
    }

    private function get_web_page(string $url, array $cookies = [], bool $isGetCookies = false)
    {
        $options = [
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER => true,    // return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING => "",       // handle all encodings
            CURLOPT_USERAGENT => "spider", // who am i
            CURLOPT_AUTOREFERER => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT => 120,      // timeout on response
            CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
        ];

        if (count($cookies) > 0) {
            $options[CURLOPT_COOKIE] = $this->cookiesToString($cookies);
        }

        $ch = curl_init($url); //initiate the Curl program that we will use to scrape data off the webpage
        curl_setopt_array($ch, $options); //set the data sent to the webpage to be readable by the webpage (JSON)
        $content = curl_exec($ch); //creates function to read pages content. This variable will be used to hold the sites html
        $err = curl_errno($ch); //errno function that saves all the locations our scraper is sent to. This is just for me so that in the case of a error,
        //I can see what parts of the page has it seen and more importantly hasnt seen
        $errmsg = curl_error($ch); //check error message function. for example if I am denied permission this string will be equal to: 404 access denied
        $header = curl_getinfo($ch); //the information of the page stored in a array
        curl_close($ch); //Closes the Curler to save site memory

        if($isGetCookies) {
            // getting cookies
            // Matching the response to extract cookie value
            preg_match_all('/^Set-Cookie:\s*([^;]*)/mi',
            $content,  $match_found);

            $cookies = [];
            foreach($match_found[1] as $item) {
                parse_str($item, $cookie);
                $cookies = array_merge($cookies,  $cookie);
            }
            $header['cookies'] = $cookies;
        }

        $header['errno'] = $err; //sending the header data to the previously made errno, which contains a array path of all the places my scraper has been
        $header['errmsg'] = $errmsg; //sending the header data to the previously made error message checker function.
        $header['content'] = $content; //sending the header data to the previously made content checker that will be the variable holder of the webpages HTML.
        return $header; //Return all the pages data and my identifying functions in a array. To be used in the presentation of the search results.
    }

    private function cookiesToString(array $cookies)
    {
        $cookiesString = "";
        $isFirst = true;
        foreach ($cookies as $key => $value) {
            if (!$isFirst) {
                $cookiesString .= "; ";
            } else {
                $isFirst = false;
            }
            $cookiesString .= $key . "=" . $value;
        }
        $this->logger->info('cookies string', [$cookiesString]);
        return $cookiesString;
    }

    private function transformPrixUnite(string $prixUnite)
    {
        $prixUnites = explode( ' €/', $prixUnite);
        $prixUnitTab = [];
        foreach ($prixUnites as $prixUnit) {
            if (strlen($prixUnit) > 3) {
                $prixUnitTab['prixUnite'] = $prixUnit;
            } else {
                $prixUnitTab['unite'] = $prixUnit;
            }
        }
        return $prixUnitTab;
    }

}