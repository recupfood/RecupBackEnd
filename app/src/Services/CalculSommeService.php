<?php

namespace App\Services;

use App\Entity\AssoProduitRecup;
use App\Entity\Recup;
use App\Repository\PrixRepository;

class CalculSommeService
{

    private PrixRepository $prixRepository;

    public function __construct(PrixRepository $prixRepository)
    {
        $this->prixRepository = $prixRepository;
    }


    public function additionSommeRecup(Recup $recup): float
    {
        $somme = 0;
        foreach ($recup->getProduits() as $produit) {

            $somme = $somme + $produit->getDernierPrix() * $produit->getQuantite();
        }
        return $somme;
    }


    public function calculLignePrix(AssoProduitRecup $assoProduitRecup): float
    {
        $produit = $assoProduitRecup->getProduit();
        $date = $assoProduitRecup->getRecup()->getDate();
        $getPrix = $this->prixRepository->getLastByProduitAndDate($produit, $date);

        if ($getPrix == null) {
            $derPrix = 0;
        } else {
            $derPrix = $getPrix['prix'];
//            $retour = $derPrix * $assoProduitRecup->getQuantite();
        }
        return $derPrix;
    }
}