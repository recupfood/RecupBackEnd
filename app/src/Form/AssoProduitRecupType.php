<?php

namespace App\Form;

use App\Entity\AssoProduitRecup;
use App\Entity\Produit;
use App\Entity\Recup;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AssoProduitRecupType extends AbstractType
{
    const IS_QUANTITE_REQUIRED = 'isQuantiteRequired';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('quantite', NumberType::class, [
                'empty_data' => ''
            ])
            ->add('produit', EntityType::class, [
                'class' => Produit::class,
                'empty_data' => ''
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AssoProduitRecup::class,
            self::IS_QUANTITE_REQUIRED => true
        ]);
    }
}
