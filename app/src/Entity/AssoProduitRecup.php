<?php

namespace App\Entity;

use App\Repository\AssoProduitRecupRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AssoProduitRecupRepository::class)]
#[ORM\UniqueConstraint(name: "asso_produit_recup_unique", columns: ["produit_id","recup_id"])]
class AssoProduitRecup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Produit::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $produit;

    #[ORM\ManyToOne(targetEntity: Recup::class, inversedBy: 'assoProduitRecups')]
    #[ORM\JoinColumn(nullable: false)]
    private $recup;

    #[Assert\NotBlank]
    #[ORM\Column(type: 'float')]
    private $quantite;

    #[ORM\Column(type: 'float', nullable: true)]
    private $dernierPrix;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    public function setProduit(?Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    public function getRecup(): ?Recup
    {
        return $this->recup;
    }

    public function setRecup(?Recup $recup): self
    {
        $this->recup = $recup;

        return $this;
    }

    public function getQuantite(): ?float
    {
        return $this->quantite;
    }

    public function setQuantite(float $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getDernierPrix(): ?float
    {
        return $this->dernierPrix;
    }

    public function setDernierPrix(?float $dernierPrix): self
    {
        $this->dernierPrix = $dernierPrix;

        return $this;
    }

    public function toArray(): ?array
    {
        return [
            'produit'       =>  $this->getProduit()->toArray(),
            'quantite'      =>  $this->getQuantite(),
            'dernierPrix'   =>  $this->getDernierPrix()
        ];
    }
}
