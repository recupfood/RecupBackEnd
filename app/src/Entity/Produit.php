<?php

namespace App\Entity;

use App\Repository\ProduitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ProduitRepository::class)]
class Produit
{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    #[ORM\Column(type: 'string', length: 255, unique: true)]
    private $nom;

    #[ORM\OneToMany(mappedBy: 'produit', targetEntity: Prix::class, orphanRemoval: true)]
    private $listePrix;

    #[Assert\NotBlank]
    #[ORM\ManyToOne(targetEntity: Unite::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $unite;

    public function __construct()
    {
        $this->listePrix = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getUnite(): ?Unite
    {
        return $this->unite;
    }

    public function setUnite(?Unite $unite): self
    {
        $this->unite = $unite;

        return $this;
    }

    /**
     * @return Collection<int, Prix>
     */
    public function getListePrix(): Collection
    {
        return $this->listePrix;
    }

    public function addListePrix(Prix $listePrix): self
    {
        if (!$this->listePrix->contains($listePrix)) {
            $this->listePrix[] = $listePrix;
            $listePrix->setProduit($this);
        }

        return $this;
    }

    public function removeListePrix(Prix $listePrix): self
    {
        if ($this->listePrix->removeElement($listePrix)) {
            // set the owning side to null (unless already changed)
            if ($listePrix->getProduit() === $this) {
                $listePrix->setProduit(null);
            }
        }

        return $this;
    }

    public function toArray(): ?array
    {
        return [
            'id'    =>   $this->getId(),
            'nom'   =>   $this->getNom(),
            'unite' =>   $this->getUnite()->getSymbole()
        ];
    }
}
