<?php

namespace App\Entity;

use App\Repository\RecupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: RecupRepository::class)]
class Recup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[ORM\Column(type: 'date')]
    private $date;

    #[ORM\OneToMany(mappedBy: 'recup', targetEntity: AssoProduitRecup::class, orphanRemoval: true, cascade: ['persist'])]
    private $produits;

    private $somme;

    public function __construct()
    {
        $this->date = new \DateTime();
        $this->produits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection<int, AssoProduitRecup>
     */
    public function getProduits(): Collection
    {
        return $this->produits;
    }

    public function addAssoProduitRecup(AssoProduitRecup $assoProduitRecup): self
    {
        if (!$this->produits->contains($assoProduitRecup)) {
            $this->produits[] = $assoProduitRecup;
            $assoProduitRecup->setRecup($this);
        }

        return $this;
    }

    public function removeAssoProduitRecup(AssoProduitRecup $assoProduitRecup): self
    {
        if ($this->produits->removeElement($assoProduitRecup)) {
            // set the owning side to null (unless already changed)
            if ($assoProduitRecup->getRecup() === $this) {
                $assoProduitRecup->setRecup(null);
            }
        }

        return $this;
    }

    public function getSomme(): ?float
    {
        return $this->somme;
    }

    public function setSomme(?float $somme): self
    {
        $this->somme = $somme;

        return $this;
    }

    public function toArray(): ?array
    {
        $array = [
            'id'    =>      $this->getId(),
            'nom'   =>      $this->getNom(),
            'date'  =>      $this->getDate(),
            'somme' =>      $this->getSomme(),
        ];
        foreach ($this->getProduits() as $assoProduitRecup) {
            $array['produits'][] = $assoProduitRecup->getProduit()->getId();
        }

        return $array;
    }
}
