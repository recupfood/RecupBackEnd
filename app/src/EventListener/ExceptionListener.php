<?php

namespace App\EventListener;

use App\Exception\ApplicationException;
use App\Exception\BadRequestException;
use App\Exception\NotFoundException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExceptionListener
{
    private $logger;
    private $parameterBag;

    public function __construct(LoggerInterface $logger, ParameterBagInterface $parameterBag)
    {
        $this->logger = $logger;
        $this->parameterBag = $parameterBag;
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        if ($event->getThrowable() instanceof ApplicationException
            || $event->getThrowable() instanceof NotFoundHttpException
            || $event->getThrowable() instanceof UniqueConstraintViolationException
        ) {
            $response = $this->handleKnownExceptions($event->getThrowable());
        } else {
            $response = $this->handleUnknownExceptions($event->getThrowable());
        }

        $event->setResponse($response);
    }

    private function handleKnownExceptions(\Throwable $exception): JsonResponse
    {
        $message = $exception->getMessage();
        $code = $exception->getCode();
        $header = [];

        if($exception instanceof NotFoundHttpException) {
            $message = $this->typeEntity($message) . " n'as pas été trouvé";
            $code = $exception->getStatusCode();
        }
        if ($exception instanceof NotFoundException) {
            $message = $exception->getMessage();
        }
        if ($exception instanceof BadRequestException) {
            $message = $exception->getMessage();
        }
        if ($exception instanceof UniqueConstraintViolationException) {
            $message = substr($exception->getMessage(),64);
            $code = Response::HTTP_CONFLICT;
        } else {
            $this->logger->error($exception);
        }

        return new JsonResponse($message, $code, $header);
    }

    private function handleUnknownExceptions(\Throwable $exception): JsonResponse
    {
        $this->logger->error($exception);
        $message = 'Une erreur inconnue à été détecté';

        // en dev on renvoie la vrai erreur
        if($this->parameterBag->get('kernel.debug')) {
            throw $exception;
        }
        return new JsonResponse($message, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * fonction qui renvoie la réponse voulu en fonction du type de l'entité
     * @param String $message
     * @return String
     */
    private function typeEntity(String $message): String {
        $retour = "";
        $type = substr($message, 11,3 );
        switch ($type) {
            case "Pro":
                $retour = "Le produit";
                break;
            case "Pri":
                $retour = "Le prix";
                break;
            case "Rec":
                $retour = "La recup";
                break;
        }
        return $retour;
    }

}