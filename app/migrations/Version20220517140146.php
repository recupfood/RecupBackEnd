<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220517140146 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE asso_produit_recup (id INT AUTO_INCREMENT NOT NULL, produit_id INT NOT NULL, recup_id INT NOT NULL, quantite DOUBLE PRECISION NOT NULL, dernier_prix DOUBLE PRECISION DEFAULT NULL, INDEX IDX_F7D73196F347EFB (produit_id), INDEX IDX_F7D731961FB48433 (recup_id), UNIQUE INDEX asso_produit_recup_unique (produit_id, recup_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prix (id INT AUTO_INCREMENT NOT NULL, produit_id INT NOT NULL, prix DOUBLE PRECISION NOT NULL, date DATE NOT NULL, INDEX IDX_F7EFEA5EF347EFB (produit_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE produit (id INT AUTO_INCREMENT NOT NULL, unite_id VARCHAR(1) NOT NULL, nom VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_29A5EC276C6E55B5 (nom), INDEX IDX_29A5EC27EC4A74AB (unite_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recup (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE unite (id VARCHAR(1) NOT NULL, nom VARCHAR(255) NOT NULL, symbole VARCHAR(5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE asso_produit_recup ADD CONSTRAINT FK_F7D73196F347EFB FOREIGN KEY (produit_id) REFERENCES produit (id)');
        $this->addSql('ALTER TABLE asso_produit_recup ADD CONSTRAINT FK_F7D731961FB48433 FOREIGN KEY (recup_id) REFERENCES recup (id)');
        $this->addSql('ALTER TABLE prix ADD CONSTRAINT FK_F7EFEA5EF347EFB FOREIGN KEY (produit_id) REFERENCES produit (id)');
        $this->addSql('ALTER TABLE produit ADD CONSTRAINT FK_29A5EC27EC4A74AB FOREIGN KEY (unite_id) REFERENCES unite (id)');
        $this->addSql('INSERT INTO unite (id, nom, symbole) VALUES ("l", "litre", "L"), ("k", "kilogramme", "Kg"), ("u", "untie", "u")');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE asso_produit_recup DROP FOREIGN KEY FK_F7D73196F347EFB');
        $this->addSql('ALTER TABLE prix DROP FOREIGN KEY FK_F7EFEA5EF347EFB');
        $this->addSql('ALTER TABLE asso_produit_recup DROP FOREIGN KEY FK_F7D731961FB48433');
        $this->addSql('ALTER TABLE produit DROP FOREIGN KEY FK_29A5EC27EC4A74AB');
        $this->addSql('DROP TABLE asso_produit_recup');
        $this->addSql('DROP TABLE prix');
        $this->addSql('DROP TABLE produit');
        $this->addSql('DROP TABLE recup');
        $this->addSql('DROP TABLE unite');
    }
}
